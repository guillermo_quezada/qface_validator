Rails.application.routes.draw do
  root 'home#index'
  get '/home', to: 'home#index'
  post '/screenshots', to: 'screenshots#create'
  resources :profiles, only: [:create]
end
