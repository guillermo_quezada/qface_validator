class AddScreenshots < ActiveRecord::Migration[6.0]
  def change
    create_table :screenshots do |t|
      t.string :name
      t.binary :image_data, limit: 2.megabytes
      t.timestamps
    end
    add_reference :screenshots, :profile, foreign_key: true
  end
end
