import numpy as np
import os,  sys
import math
from matplotlib import pyplot as plt
import cv2
import base64
print(cv2.__version__)

people_path  = sys.argv[1]
results_path = sys.argv[2]
profile_name = sys.argv[3]
frontalface_xml = sys.argv[4]
profile_image   = sys.argv[5]
grayscale_path  = sys.argv[6]

frame = cv2.imread(profile_image)

detector = cv2.CascadeClassifier(frontalface_xml)
scale_factor = 1.2
min_neighbors = 5
min_size = (1, 1)
biggest_only = True
flags = cv2.CASCADE_FIND_BIGGEST_OBJECT | \
        cv2.CASCADE_DO_ROUGH_SEARCH if biggest_only else \
        cv2.CASCADE_SCALE_IMAGE

def cut_faces(image, faces_coord):
    faces = []
    for (x, y, w, h) in faces_coord:
        w_rm = int(0.2 * w / 2)
        faces.append(image[y: y + h, x + w_rm: x + w - w_rm])
    return faces

def normalize_intensity(images):
    images_norm = []
    for image in images:
        is_color = len(image.shape) == 3
        if is_color:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        images_norm.append(cv2.equalizeHist(image))
    return images_norm

def resize(images, size=(50,50)):
    images_norm = []
    for image in images:
        if image.shape < size:
            image_norm = cv2.resize(image, size, interpolation = cv2.INTER_AREA)
        else:
            image_norm = cv2.resize(image, size, interpolation = cv2.INTER_CUBIC)
        images_norm.append(image_norm)
    return images_norm


def collect_dataset():
    images = []
    labels = []
    labels_dic = {}
    people = [person for person in os.listdir(grayscale_path)]
    for i, person in enumerate(people):
        labels_dic[i] = person
        for image in os.listdir(grayscale_path+person):
            images.append(cv2.imread(grayscale_path+person+'/'+image,0))
            labels.append(i)
    return (images, np.array(labels), labels_dic)

def draw_rectangle(image, coords):
    for [x, y, w, h] in coords:
        cv2.rectangle(image, (x, y), (x + w, y + h), (150, 150, 0), 8)

def normalize_faces(frame, faces_coord):
    faces = cut_faces(frame, faces_coord)
    faces = normalize_intensity(faces)
    faces = resize(faces)
    return faces

def pre_process_images():
    people = [person for person in os.listdir(people_path)]
    counter = 0
    for i, person in enumerate(people):
        for image in os.listdir(people_path+person):
            frame = cv2.imread(people_path+person+'/'+image, 0)
            faces_coord = detector.detectMultiScale(frame,
                            scaleFactor=scale_factor,
                            minNeighbors=min_neighbors,
                            minSize=min_size,
                            flags=flags)
            if len(faces_coord):
                faces = normalize_faces(frame, faces_coord)
                cv2.imwrite(grayscale_path + person + "/" + str(counter) + '.jpg', faces[0])
                counter += 1

pre_process_images()

images, labels, labels_dic = collect_dataset()
rec_eig = cv2.face.LBPHFaceRecognizer_create()
rec_eig.train(images, labels)
print("Models trained")

faces_coord = detector.detectMultiScale(frame,
			scaleFactor=scale_factor,
			minNeighbors=min_neighbors,
			minSize=min_size,
			flags=flags)

faces = normalize_faces(frame, faces_coord)

collector = cv2.face.StandardCollector_create()
rec_eig.predict_collect(faces[0], collector)
conf = collector.getMinDist()
label = collector.getMinLabel()
threshold = 142
print("Confidence: " + str(conf))
if conf < threshold:
   # print(labels_dic[label])
   print("Verified!")
   cv2.putText(frame, "Verified!", (faces_coord[0][0], faces_coord[0][1] - 10), cv2.FONT_HERSHEY_PLAIN,3,(66,53,243), 2)
else:
   print('Incorrect')
   cv2.putText(frame, 'Incorrect', (faces_coord[0][0], faces_coord[0][1] - 10), cv2.FONT_HERSHEY_PLAIN,3,(66,53,243), 2)

draw_rectangle(frame, faces_coord)
cv2.imwrite(results_path + profile_name + '.jpg', frame)
