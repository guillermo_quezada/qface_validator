module Screenshots
  class WriteService < BaseService
    def initialize(py_argv)
      @py_argv = py_argv
    end

    def process
      write_screeshots
    end

    private

    def write_screeshots
      profile = Profile.find_by(name: @py_argv.session_id)
      profile.screenshots.each_with_index do |screenshot, index|
        File.open(@py_argv.tmp_profiles + "/#{index}.jpg",'wb') { |file| file.write(Base64.decode64(screenshot.image_data)) }
      end
    end
  end
end
