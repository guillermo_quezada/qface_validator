module Screenshots
  class CreateService < BaseService

    def initialize(session, screenshots)
      @session = session
      @screenshots  = screenshots[:screenshots]
    end

    def process
      create_screenshots
    end

    private

    def create_screenshots
      name   = @session.id
      profile = Profile.find_or_create_by(name: name)
      profile.screenshots.destroy_all
      @screenshots.each do |image|
        binary = image.split(',').last
        profile.screenshots.create(name: name, image_data: binary)
      end
    end
  end
end
