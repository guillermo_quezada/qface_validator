class BaseService
  def self.for(*args)
    new(*args).process
  end
end
