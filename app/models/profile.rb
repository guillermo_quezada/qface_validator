class Profile < ApplicationRecord
  has_many :screenshots, dependent: :destroy
  has_one_attached :image, dependent: :destroy
end
