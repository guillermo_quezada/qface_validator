module FaceRecognition
  class AnalizeJob < ApplicationJob
    include Rails.application.routes.url_helpers
    def perform(session_id)
			profile_image = Profile.find_by(name: session_id).image
      py_argv = PythonArgv.new(session_id, profile_image)
			system_clear(py_argv)
      system_setup(py_argv)
			Screenshots::WriteService.for(py_argv)
      Rails.logger.info("python3 #{py_argv.absolute_paths}")
      success = system("python3 #{py_argv.absolute_paths}")
      ActionCable.server.broadcast "room_#{session_id}", match: match_url(session_id, success)
    end

    private

    def match_url(session_id, success)
			if success
      	Rails.application.routes.default_url_options[:host] + "/results/" + session_id + ".jpg"
			else
      	Rails.application.routes.default_url_options[:host] + "/no_face.png"
			end
    end

    def system_setup(py_argv)
      system("mkdir -p #{py_argv.tmp_profiles}")
      system("mkdir -p #{py_argv.tmp_grayscale}")
    end

		def system_clear(py_argv)
      system("rm -rf #{py_argv.tmp_grayscale}")
		end

  end
end
