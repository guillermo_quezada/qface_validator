import React from "react";
import axios from 'axios'
import Webcam from "react-webcam";
import Api from '../utils/api';
import { passCsrfToken } from '../utils/helpers';
import { Button } from 'antd';
import { Row, Col } from 'antd';
import { Result } from 'antd';
import { Spin } from 'antd';

class WebCam extends React.Component {
  constructor() {
    super();
    this.state = {
      screenshots: [],
      current: 0
    }
    this.handleClick = this.handleClick.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange(current){
    console.log('onChange:', current);
    this.setState({ current  });
  };
  handleClick(){
    var me = this;
    var counter = 0;
    var screenshots = []
		var recognition_button = document.getElementById("recognition_button")
		recognition_button.style.display = "none"
		var spinner = document.getElementById("capture_spinner")
		spinner.style.display = "flex"

    var timer = setInterval(function(){
      var screenshot = me.refs.webcam.getScreenshot();
      screenshots.push(screenshot)
      me.setState({screenshots: [...screenshots]});
      document.getElementById("progressBar").value = 19 - counter;
      if(counter >= 19){
        clearInterval(timer);
        Api.POST_IMAGES(screenshots, function(response){
          console.log(response)
        })
      }
      counter +=1;
    }, 500);
  }
  componentDidMount() {
    passCsrfToken(document, axios)
  }

  render () {
    const videoConstraints = {
      width: 180,
      height: 180,
      facingMode: "user"
    };
    const { current } = this.state;
    return (
      <div>
        <Row type="flex" justify="space-around">
          <Col span={12}>
            <Result
              title="QFace Validator Prototype!"
              subTitle="Face detection and recognition!"
            />
          </Col>
        </Row>
        <Row type="flex" justify="space-around">
          <Col span={12}>
            <Webcam
              audio={false}
              ref='webcam'
              videoConstraints={videoConstraints}
              screenshotFormat="image/jpeg"/>
          </Col>
        </Row>
        <Row type="flex" justify="space-around">
          <Col span={12}>
            <progress value="0" max="19" id="progressBar" style={{width: '50%'}}></progress>
          </Col>
        </Row>
        <Row id="recognition_button" type="flex" justify="space-around">
          <Col span={12}>
            <Button onClick={this.handleClick} size="large" type="primary">Start recognition</Button>
          </Col>
        </Row>
        <Row id="capture_spinner" type="flex" justify="space-around">
          <Col span={12}>
						<Spin size="large" />
          </Col>
        </Row>
      </div>
    );
  }
}

export default WebCam
