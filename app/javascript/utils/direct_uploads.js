import consumer from "../channels/consumer"
import { message } from 'antd';

const successUpload = () => {
  message.success('Selfie uploaded');
	var recognition_button = document.getElementById("recognition_button")
	recognition_button.style.display = "flex"
};

const errorUpload = () => {
  message.error('Selfie cannot be empty');
};

addEventListener("direct-upload:initialize", event => {
  const { target, detail } = event
  const { id, file } = detail
  target.insertAdjacentHTML("beforebegin", `
    <div id="direct-upload-${id}" class="direct-upload direct-upload--pending">
      <div id="direct-upload-progress-${id}" class="direct-upload__progress" style="width: 0%"></div>
      <span class="direct-upload__filename">${file.name}</span>
    </div>
  `)
})

addEventListener("direct-upload:start", event => {
  const { id } = event.detail
  const element = document.getElementById(`direct-upload-${id}`)
  element.classList.remove("direct-upload--pending")
})

addEventListener("direct-upload:progress", event => {
  const { id, progress } = event.detail
  const progressElement = document.getElementById(`direct-upload-progress-${id}`)
  progressElement.style.width = `${progress}%`
})

addEventListener("direct-upload:error", event => {
  event.preventDefault()
  const { id, error } = event.detail
  const element = document.getElementById(`direct-upload-${id}`)
  element.classList.add("direct-upload--error")
  element.setAttribute("title", error)
})

addEventListener("direct-upload:end", (event, x) => {
	console.log(event)
  const { id } = event.detail
  const element = document.getElementById(`direct-upload-${id}`)
  element.classList.add("direct-upload--complete")
})

document.addEventListener('DOMContentLoaded', function(){
	var form_profile = document.getElementById('new_profile');
  var result_profile = document.getElementById('result');
  form_profile.addEventListener('ajax:error', function(response){
    errorUpload();
	})
  form_profile.addEventListener('ajax:success', function(response){
    successUpload();
		const room = response.detail[0].id
		consumer.subscriptions.create({channel: 'WebFaceNotificationChannel', room: room}, {
			received(data) {
				var spinner = document.getElementById("capture_spinner")
				spinner.style.display = "none"
				result_profile.src = data.match
			}
		});
	})
})

