import axios from 'axios'

const Api = {
  POST_IMAGES: function(screenshots, request){
    const endpoint = '/screenshots';
    axios
      .post(endpoint, {screenshots})
      .then(response => {
        request(response);
      })
  }
}

export default Api
