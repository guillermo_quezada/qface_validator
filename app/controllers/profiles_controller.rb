class ProfilesController < ApplicationController
  def create
		@profile = Profile.find_or_create_by(name: session.id)
    @profile.image.attach(profile_params[:image])
		render json: {id: session.id }, status: 200
  end

	private

	def profile_params
		params.require(:profile).permit(:image)
	end
end
