class ScreenshotsController < ApplicationController

  def create
    Screenshots::CreateService.for(session, screenshot_params)
		FaceRecognition::AnalizeJob.perform_later(session.id)
  end

  private

  def screenshot_params
    params.permit(screenshots: [])
  end
end
