class PythonArgv
  attr_reader :profiles_path,
              :grayscale_path,
              :results_path,
              :qface_exec,
              :frontalface_xml,
              :tmp_profiles,
              :tmp_grayscale,
              :session_id

  def initialize(session_id, profile_image)
    @profiles_path   = ENV['PYTHON_PROFILES_PATH'] + "room_#{session_id}/"
    @grayscale_path  = ENV['PYTHON_GRAYSCALE_PATH'] + "room_#{session_id}/"
    @results_path    = ENV['PYTHON_RESULTS_PATH']
    @qface_exec      = ENV['PYTHON_QFACE_EXEC']
    @frontalface_xml = ENV['PYTHON_FRONTALFACE_XML']
    @tmp_profiles    = @profiles_path  + session_id
    @tmp_grayscale   = @grayscale_path + session_id
    @session_id      = session_id
    @profile_image   = profile_image
  end

  def absolute_paths
    "#{absolute_path(@qface_exec)}" +
    " #{absolute_path(@profiles_path)}" +
    " #{absolute_path(@results_path)}" +
    " #{@session_id}" +
    " #{absolute_path(@frontalface_xml)}" +
    " #{ActiveStorage::Blob.service.path_for(@profile_image.key)}" +
    " #{absolute_path(@grayscale_path)}"
  end

  private

  def absolute_path(relative_path)
    Rails.root.to_s + "/" + relative_path
  end
end
